﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_3 : MonoBehaviour {

	public Vector2 velocity;

	private Rigidbody2D rigid;

	// Use this for initialization
	void Start () {
		rigid = GetComponent<Rigidbody2D>();

		rigid.AddForce(velocity, ForceMode2D.Impulse);
		
	}
	
}
